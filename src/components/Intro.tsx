import { AiFillGitlab, AiFillLinkedin, AiOutlineMail } from "react-icons/ai";

export const Intro = () => {
  return (
    <div>
      <h3 className="text-2xl mt-16">Hi! I'm</h3>
      <h1 className="text-5xl mt-4">Robbie Tambunting</h1>
      <h2 className="text-3xl mt-3">Full Stack Software Engineer</h2>
      <p className="mt-10">
        I'm a Software Engineer who loves to create visually appealing sites!
        While pursuing my bachelor's degree in Computational Mathematics, I took
        an Intro to Python course that opened my eyes to the possibilities of
        programming! Since then, software projects have allowed me to combine
        technical knowledge with my creative side.{" "}
      </p>
      <a className="rounded-xl border-2 p-3 mt-10 inline-block" href="">
        Resume
      </a>
      <div className="flex">
        <a href="https://gitlab.com/rtambunt">
          <AiFillGitlab size={40} color={"#FC6D27"} />
        </a>
        <a href="https://www.linkedin.com/in/robbie-tambunting/">
          <AiFillLinkedin size={40} color={"#0072B1"} />
        </a>
        <a href="mailto:rtambunting@gmail.com">
          <AiOutlineMail size={40} color={"#55555"} />
        </a>
      </div>
    </div>
  );
};
